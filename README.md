**Notice:** This project is largely defunct as it was work I was doing on my spare time to use at my job before the move was made to other software (in other words, SCM Manager is no longer used). This project now largely exists as an exercise in building a command-line application in Node and **is not** suitable for production use at the moment.
---

# SCM-CLI - A Command-line Client for SCM Manager

SCM-CLI is a fully-featured command-line client for SCM Manager. SCM-CLI's bread and butter is working with your repositories, providing commands for viewing, cloning, and more. SCM-CLI also allows users to view details about repository groups and user details, all with a simple set of commands.

SCM-CLI is built in JavaScript/Nodejs.

## Installing SCM-CLI
SCM-CLI is currently *not* on NPM, so to install it, you can simply clone the directory and run `npm install`:

```
$ git clone https://bitbucket.org/mgerton/scm-cli
$ cd scm-cli
$ npm install -g
```

If you're not using a tool like NVM in Linux, you'll have to prepend `sudo` to your install command.

## Getting Started
After installing SCM-CLI, you'll need to login:

```
$ scmman login
```

You will be prompted to enter the domain of your SCM Manager instance as well as your username and password. The domain is only required upon initial setup, but can be changed via the `scmman domain` command.

### Building Your Repository List
Once you are logged in, you can see all of the repositories that you have access to:

```
$ scmman list
```

When this command first runs, SCM-CLI caches a local list of these repositories to save API calls. If your list is ever outdated, you can pass the `update` flag to grab whatever you're missing:

```
$ scmman list --update
```

### Working with Repositories
SCM-CLI's main interaction will be with your repositories. SCM-CLI provides a basic view command via `scmman repo`, which shows the basic details of a repository:

```
$ scmman repo my-project
```

SCM-CLI will also allow you to clone a project, wrapping the command of the the SCM tool that is used for a given project (SCM-CLI supports Mercurial to start, but work is planned for Git and SVN):

```
$ scmman clone my-project
```

the `clone` command also supports groups, if you use them to organize your projects:

```
$ scmman clone my-team/my-project
```

## CLI Reference
**Note:** SCM-CLI is still in-development, so commands and arguments are subject to change.

  1. General commands
    - `scmman`
        - throws an error with nothing
    - `scmman --help`
        - list the available commands and options
    - `scmman -V` or `scmman --version`
        - shows the current version of SCM-CLI
  1. Authentication commands
    - `scmman login`
        - prompts a login screen. Required so that `scmman list --update` works since SCM Manager requires authentication
    - `scmman logout`
        - logs the user out of the current SCM Manager session in use by SCM-CLI
  1. Config commands
    - `scmman domain`
        - Prompts the user to update their domain, which is used as the root for repository commands
  1. Repository commands
    - `scmman clone <repo_name>`
        - clones the repo of the specified name to the default target directory
        - should also honor any additional hg commands (roadmap: git/svn support)
    - `scmman list`
        - lists all known repository URLs
    - `scmman list -u` or `scmman list --update`
        - updates the list of repository URLs
    - `scmman repo <repo_name>`
        - shows the details for a given repository

## Future Improvements/Roadmap

  - Better repo searching via some sort of fuzzy search logic (so specifying a full path isn't required)
    - wrinkle with this is resolving repos names in different locations
  - Multiple SCM support (Git, SVN)
  - Better support on Windows(?)
  - Better UI for list view
  - More options for `list` command
      - limit
      - sort
      - etc.

## Developer Notes

  - The list contents will be stored in a dotfile in the user's local filesystem (`~/.scm_cli/repo_list`)
  - **TODO:** Update repo name to reflect project name change
  - **TODO:** store auth credentials for requests to persist [in-progress]
  - **TODO:** check logged-in status prior to running each command; show a nice message when not logged in
  - **TODO:** post-install script that:
      - creates the dotfolder (`.scm_cli`)
      - runs any additional bootstrapping commands