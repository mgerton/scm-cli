/**
 * Required modules
 */

// config module (always should be first)
var config = require('./config');

// lodash and custom mixins
var _ = require('lodash');
var mixins = require('./mixins');

// 3rd part deps
var request = require('request');
var prompt = require('prompt');
var chalk = require('chalk');

// Native Node modules
var fs = require('fs');

// module-specific vars
var userHome = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
var cookieJar = request.jar();
var configFiles = {
	auth: userHome + '/.scm_cli/auth.json',
	repoList: userHome + '/.scm_cli/repo_list.json'
};

/**
 * command functions
 */

/**
 * Lists all of the available repositories
 * @param  {Object} options The set of command line args passed in
 */
exports.list = function list(options) {
	// console.log(scmman.args);
	// console.log(cookieJar);
	var repos = JSON.parse(require(userHome + '/.scm_cli/repo_list'));
	var auth = require(userHome + '/.scm_cli/auth');

	if (options.update) {
		// TODO extract this?
		var apiUrl = [
			config.scmManager.BASE_URL,
			config.scmManager.api.BASE_URI,
			config.scmManager.api.REPOSITORIES,
			config.scmManager.api.EXTENSION
		].join('');

		// TODO: clean me up
		var j = request.jar();
		var authCookie = request.cookie(auth.cookie);
		j.setCookie(authCookie, apiUrl);

		request({ url: apiUrl, jar: j }, function(error, response, body) {
			if (error) {
				console.log(chalk.red('error fetching repository list: ', error));
			} else {
				console.log(chalk.white('updating local repository list...'));
				fs.writeFile(userHome + '/.scm_cli/repo_list.json', JSON.stringify(body), function(error) {
					var messages = {
						error: chalk.red('error updating list: ', error),
						success: chalk.green.bold('list updated')
					};

					if (error) {
						console.log(messages.error);
					} else {
						console.log(messages.success);
					}
				});
			}
		});
	} else {
		console.log(chalk.green.bold.underline('Available Repositories:'));
		repos.forEach(function(repo) {
			var group = options.group ? options.group.replace(/_/, '-').toLowerCase() + '/' : '';

			if (_.startsWith(repo.name, group)) {
				console.log('  ' + chalk.white(repo.name));
			}
		});
	}
};

/**
 * Shows details for a repository
 * @param  {String} repoName The slug of a repository (project/repo-name)
 */
exports.getRepoDetails = function getRepoDetails(repoName) {
	var repos = require(userHome + '/.scm_cli/repo_list');
	console.log(chalk.green.bold.underline('Details for ' + repoName + ':'));

	var repo = repos.filter(function(details) {
		return details.name === repoName;
	});

	repo.forEach(function(details) {
		// remove any falsey keys for displaying
		// details = _.compactObject(details);
		details = _.omit(details, _.isEmpty);

		_.forOwn(details, function(detail, key) {
			key = _.startCase(key);

			if (typeof detail === 'string') {
				detail = detail.replace(/\n/, ' ');
			}

			console.log(chalk.white.bold('  ' + key + ': ') + chalk.white(detail));
		});
	});
};

/**
 * Logs a user into SCM Manager
 */
exports.login = function login() {
	var user = '';
	var password = '';
	var apiUrl = [
		config.scmManager.BASE_URL,
		config.scmManager.api.BASE_URI,
		config.scmManager.api.AUTH_LOGIN,
		config.scmManager.api.EXTENSION
	].join('');

	var schema = {
		properties: {
			username: {
				description: chalk.white.bold('Username:'),
				required: true
			},
			password: {
				description: chalk.white.bold('Password:'),
				hidden: true
			}
		}
	};

	console.log(chalk.green.bold('LDAP Login'));
	prompt.start();
	prompt.get(schema, function (err, result) {
		user = result.username;
		password = result.password;

		request.post({
			url: apiUrl,
			form: {
				username: user,
				password: password,
				rememberMe: true
			}
		}, function(error, response, body) {
			var responseCookies = response.headers['set-cookie'];
			var loginCookie;

			// TODO: move this elsewhere
			function saveCredentials() {
				fs.writeFile(configFiles.auth, JSON.stringify({ cookie: loginCookie.toString() }) + '\n', function(error) {
					var messages = {
						error: chalk.red('error trying to save credentials: ', error),
						success: chalk.green('credentials saved')
					};

					console.log(error);
					// var output = error ? messages.error : messages.success;
					// console.log(output);
				});
			}

			if (error) {
				return console.error(chalk.red('auth failed: ', error));
			}

			// Grab the cookie from the response so we can pass it along with any future requests and
			// scrub the 'deleteMe' cookie if it's present
			loginCookie = responseCookies.filter(function(cookie) {
				return cookie.indexOf('rememberMe=deleteMe') < 0 && cookie.indexOf('rememberMe=') > -1;
			})[0].split(';')[0];

			// console.log(loginCookie);
			// console.log(JSON.stringify(loginCookie));

			// FIXME: this logic still needs some TLC

			// store the cookie in a config file so it persists across commands
			if (fs.existsSync(configFiles.auth)) {
				saveCredentials();
			} else {
				// create file THEN save creds
				fs.closeSync(fs.openSync(configFiles.auth, 'wx'));
				saveCredentials();
			}

			// TODO extract this out since it will have to be used in each command
			var authData = fs.readFileSync(configFiles.auth);

			try {
				authCookie = request.cookie(JSON.parse(authData).cookie);
				cookieJar.setCookie(authCookie, config.scmManager.BASE_URL);
				console.log(cookieJar);

				console.log(chalk.green.bold('login successful'));
			} catch (e) {
				console.error(chalk.red('error trying to load credentials: ', e));
			}
		});
	});
};

/**
 * Logs a user out of SCM Manager
 */
exports.logout = function logout() {
	var apiUrl = [
		config.scmManager.BASE_URL,
		config.scmManager.api.BASE_URI,
		config.scmManager.api.AUTH_LOGOUT,
		config.scmManager.api.EXTENSION
	].join('');

	console.log(apiUrl);

	request.post(apiUrl, function(error, response, body) {
		fs.writeFile(configFiles.auth, JSON.stringify({}), function(error) {
			var messages = {
				error: chalk.red('error trying to logout: ', error),
			};

			if (error) {
				console.log(messages.error);
			} else {
				console.log(cookieJar);
				console.log(chalk.green.bold('logout successful'));
			}
		});
	});
};