/**
 * A collection of lodash mixins used by Sclim
 */

var _ = require('lodash');

/**
 * Removes falsey keys from an object, like _.compact for Arrays
 * @see [StackOverflow](http://stackoverflow.com/questions/14058193/remove-empty-properties-falsy-values-from-object-with-underscore-js)
 * for more details.
 *
 * @param  {Object} obj The object to clean
 * @return {Object}     The new object sans falsey keys
 *
 * @example
 *
 * var dirtyObj = { 'foo': 'bar', '2': '', 'baz': {} };
 * _.compactObject(dirtyObj)
 * // => { 'foo': 'bar' }
 */
function compactObject(obj) {
	var clone = _.clone(obj);

	_.each(clone, function(v, k) {
		if (!v) {
			delete clone[k];
		}
	});

	return clone;
}
// Register lodash mixin
_.mixin({ 'compactObject': compactObject });
