#!/usr/bin/env node

// TODO List
// - Finish authentication
// - Store the resulting cookie value in a file in the hidden .scm_cli directory to enable persistance across multiple commands
// - Startup/init script (or command) to create the hidden directory, and general setup
// - Look into using Passport for authentication
// - Properly parse repo list
// - clone!

// A title for the process when running `ps`
process.title = 'scmman';

// The main application
var scmman = require('commander');
var version = require('../package.json').version;

// commands module
var commands = require('./commands');

/**
 * SCM-CLI Bootstrapping
 */

// Expose the version on the CLI
scmman.version(version);

scmman
	.usage('[command] [options]');

// repo list command
scmman
	.command('list')
	.description('list all available repositories')
	.option('-u, --update', 'get the latest list of repositories')
	.option('-g, --group <name>', 'show the respositories for a particular group')
	.action(commands.list);

// repo details command
scmman
	.command('repo [name]')
	.description('show details for a specified repository')
	.action(commands.getRepoDetails);

// login command
scmman
	.command('login')
	.description('logs a user into SCM Manager based on credentials')
	.action(commands.login);

// logout command
scmman
	.command('logout')
	.description('logs a user out of SCM Manager')
	.action(commands.logout);

// Parses the args and off we go!
// TODO: Show the help dialog if no arguments are passed
// 	console.log(chalk.red('Error: please specify a command. Run `scmman --help` for more information.'));
scmman.parse(process.argv);

// catch-all for errors
// scmman
// 	.command('*')
// 	.action(function(env) {
// 		chalk.red('Error: please specify a valid command. See `scmman --help` for more information.');
// 		terminate(true);
// 	});
