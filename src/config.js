// Required modules
var request = require('request');
var prompt = require('prompt');

// Remove the 'prompt:' prefix for commands requiring input
prompt.message = '';
prompt.delimiter = '';

// Set a custom cookie store for keeping a user logged in
request.defaults({ jar: true });

exports.scmManager = {
	// TODO: make this configurable
	BASE_URL: 'https://scm.dev.norvax.net/scm',

	// Repo URIs
	hg: {
		BASE_URI: '/hg',
	},

	// API endpoints
	api: {
		BASE_URI: '/api/rest',
		EXTENSION: '.json',
		AUTH_LOGIN: '/authentication/login',
		AUTH_LOGOUT: '/authentication/logout',
		REPOSITORIES: '/repositories'
	}
};